using System;
using System.Configuration;
using System.Reflection;
using WindowsFormsApplication1.Properties;

namespace WindowsFormsApplication1
{
	public static class QuickSettings
	{
		public static string ApplicationName;

		public static string FilePath;

		public static string FolderPath;

		public static string FileName;

		private static QuickSettings.Mode DocType;

		private static string ProgramDataDir;

		static QuickSettings()
		{
			QuickSettings.DocType = QuickSettings.Mode.Ini;
			QuickSettings.ApplicationName = Settings.Default.ApplicationName;
			QuickSettings.FileName = "Setup.Ini";
			QuickSettings.FolderPath = Assembly.GetExecutingAssembly().Location.Replace(string.Concat(Assembly.GetExecutingAssembly().GetName().Name, ".exe"), string.Empty);
			QuickSettings.FilePath = string.Concat(QuickSettings.FolderPath, QuickSettings.FileName);
		}

		public static object Get(string Section, string Key, string CustomDefaultValue)
		{
			object obj;
			switch (QuickSettings.DocType)
			{
				case QuickSettings.Mode.Ini:
				{
					obj = (new IniSettings()).Get(Section, Key, CustomDefaultValue);
					break;
				}
				case QuickSettings.Mode.Xml:
				{
					obj = (new XmlSettings()).Get(Section, Key, CustomDefaultValue);
					break;
				}
				default:
				{
					obj = null;
					break;
				}
			}
			return obj;
		}

		public static object Get(string Section, string Key, SettingsProperty Property)
		{
			object obj;
			switch (QuickSettings.DocType)
			{
				case QuickSettings.Mode.Ini:
				{
					obj = (new IniSettings()).Get(Section, Key, Property);
					break;
				}
				case QuickSettings.Mode.Xml:
				{
					obj = (new XmlSettings()).Get(Section, Key, Property);
					break;
				}
				default:
				{
					obj = null;
					break;
				}
			}
			return obj;
		}

		public static object Get(SettingsProperty Property)
		{
			object obj;
			switch (QuickSettings.DocType)
			{
				case QuickSettings.Mode.Ini:
				{
					obj = (new IniSettings()).Get(Property);
					break;
				}
				case QuickSettings.Mode.Xml:
				{
					obj = (new XmlSettings()).Get(Property);
					break;
				}
				default:
				{
					obj = null;
					break;
				}
			}
			return obj;
		}

		private static string getDataFolder()
		{
			return "";
		}

		public static void Set(string Section, string Key, string Value)
		{
			switch (QuickSettings.DocType)
			{
				case QuickSettings.Mode.Ini:
				{
					(new IniSettings()).Set(Section, Key, Value);
					break;
				}
				case QuickSettings.Mode.Xml:
				{
					(new XmlSettings()).Set(Section, Key, Value);
					break;
				}
			}
		}

		public static void Set(SettingsPropertyValue Item)
		{
			switch (QuickSettings.DocType)
			{
				case QuickSettings.Mode.Ini:
				{
					(new IniSettings()).Set(Item);
					break;
				}
				case QuickSettings.Mode.Xml:
				{
					(new XmlSettings()).Set(Item);
					break;
				}
			}
		}

		private enum Mode
		{
			Ini,
			Xml
		}
	}
}
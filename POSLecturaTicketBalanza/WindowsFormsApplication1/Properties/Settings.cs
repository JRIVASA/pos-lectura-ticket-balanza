using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using WindowsFormsApplication1;

namespace WindowsFormsApplication1.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
	public sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance;

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("TicketBalanza")]
		public string ApplicationName
		{
			get
			{
				return (string)this["ApplicationName"];
			}
		}

		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("POS_LOCAL")]
		[UserScopedSetting]
		public string LocalDB_SQLDBName
		{
			get
			{
				return (string)this["LocalDB_SQLDBName"];
			}
			set
			{
				this["LocalDB_SQLDBName"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string LocalDB_SQLPass
		{
			get
			{
				return (string)this["LocalDB_SQLPass"];
			}
			set
			{
				this["LocalDB_SQLPass"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue(".")]
		[UserScopedSetting]
		public string LocalDB_SQLServerName
		{
			get
			{
				return (string)this["LocalDB_SQLServerName"];
			}
			set
			{
				this["LocalDB_SQLServerName"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("sa")]
		[UserScopedSetting]
		public string LocalDB_SQLUser
		{
			get
			{
				return (string)this["LocalDB_SQLUser"];
			}
			set
			{
				this["LocalDB_SQLUser"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool LocalEscaner
		{
			get
			{
				return (bool)this["LocalEscaner"];
			}
			set
			{
				this["LocalEscaner"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string LocalMDB
		{
			get
			{
				return (string)this["LocalMDB"];
			}
			set
			{
				this["LocalMDB"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("7")]
		[UserScopedSetting]
		public int LocalPosFin
		{
			get
			{
				return (int)this["LocalPosFin"];
			}
			set
			{
				this["LocalPosFin"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("3")]
		[UserScopedSetting]
		public int LocalPosIni
		{
			get
			{
				return (int)this["LocalPosIni"];
			}
			set
			{
				this["LocalPosIni"] = value;
			}
		}

        [DebuggerNonUserCode]
        [DefaultSettingValue(".")]
        [UserScopedSetting]
        public string LocalDB_SQLCASServerName
        {
            get
            {
                return (string)this["LocalDB_SQLCASServerName"];
            }
            set
            {
                this["LocalDB_SQLCASServerName"] = value;
            }
        }

        [DebuggerNonUserCode]
        [DefaultSettingValue("STELLAR_BALANZAS")]
        [UserScopedSetting]
        public string LocalDB_SQLCASDBName
        {
            get
            {
                return (string)this["LocalDB_SQLCASDBName"];
            }
            set
            {
                this["LocalDB_SQLCASDBName"] = value;
            }
        }

        [DebuggerNonUserCode]
        [DefaultSettingValue("sa")]
        [UserScopedSetting]
        public string LocalDB_SQLCASUser
        {
            get
            {
                return (string)this["LocalDB_SQLCASUser"];
            }
            set
            {
                this["LocalDB_SQLCASUser"] = value;
            }
        }

        [DebuggerNonUserCode]
        [DefaultSettingValue("")]
        [UserScopedSetting]
        public string LocalDB_SQLCASPass
        {
            get
            {
                return (string)this["LocalDB_SQLCASPass"];
            }
            set
            {
                this["LocalDB_SQLCASPass"] = value;
            }
        }

        [DebuggerNonUserCode]
        [DefaultSettingValue("0")]
        [UserScopedSetting]
        public int Modalidad
        {
            get
            {
                return (int)this["Modalidad"];
            }
            set
            {
                this["Modalidad"] = value;
            }
        }

        static Settings()
		{
			Settings.defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		}

		public Settings()
		{
			base.SettingsSaving += new SettingsSavingEventHandler(this.SettingsSavingEventHandler);
		}

		private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
		{
		}

		private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
		{
			foreach (SettingsPropertyValue propertyValue in this.PropertyValues)
			{
				if (propertyValue.IsDirty)
				{
					propertyValue.KeepValue();
					propertyValue.IsDirty = false;
				}
			}
			e.Cancel = true;
		}
	}
}
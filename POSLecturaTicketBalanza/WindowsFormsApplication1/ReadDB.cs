using ADODB;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using WindowsFormsApplication1.Properties;

namespace WindowsFormsApplication1
{
	public class ReadDB : Form
	{
		private OleDbConnection Conn = new OleDbConnection();

		private DataSet Read;

		private string Gcodigo;

		private string Gnombre;

		private string Gtprecio;

		private string Grif;

		private string Gcu_direccion_cliente;

		private string Gbs_contribuyente;

		private string Gid_hijo;

		private string Gbimpuesto;

		private string GModalidad;

		private string GNumeroFactura;

        private bool param = false;

		private IContainer components = null;

		private Button btnImportar;

		private Label lblPesada;

		private TextBox txtBoxNumero;

		private DataGridView dataGrid;

		private SqlConnection CASDBConnection = new SqlConnection();

		public ReadDB()
		{
			this.InitializeComponent();
		}

		private void AjustarGrid()
		{
			try
			{
				this.dataGrid.BackgroundColor = this.dataGrid.DefaultCellStyle.BackColor;
				this.dataGrid.Visible = true;
				for (int i = 0; i < this.dataGrid.Rows.Count; i++)
				{
					int num = i + 1;
					this.dataGrid.Rows[i].HeaderCell.Value = num.ToString();
				}
                this.dataGrid.Font = new Font("Tahoma", 12);
                this.dataGrid.RowsDefaultCellStyle.Font = this.dataGrid.Font;
			}
			catch (Exception exception)
			{
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
            if (this.dataGrid.Rows.Count == 0)
            {
                MessageBox.Show("No ha cargado ningún producto.");
                txtBoxNumero.Focus();
                return;
            }

			SqlTransaction sqlTransaction = null;
			double num = 0;
			double num1 = 0;
			double num2 = 0;
			Functions function = new Functions();
			string str = "";
			try
			{
				function.connectionString.Open();
				string str1 = "";
				string str2 = "";
				double num3 = 0;
				string str3 = "";
				double num4 = 0;
				double num5 = 0;
				sqlTransaction = function.connectionString.BeginTransaction();
				for (int i = 0; i < this.dataGrid.Rows.Count; i++)
				{
					//int num6 = Convert.ToInt32(this.Read.Tables[0].Rows[i]["PROVEEDOR"]);
					string strprov = Convert.ToString(this.Read.Tables[0].Rows[i]["PROVEEDOR"]).Trim();
					string str4 = this.Read.Tables[0].Rows[i]["TICKET"].ToString();
					string str5 = this.Read.Tables[0].Rows[i]["PLU"].ToString();
					DateTime dateTime = Convert.ToDateTime(string.Concat(string.Format("{0:MM/dd/yyyy}", this.Read.Tables[0].Rows[i]["FECHA"]), " ", this.Read.Tables[0].Rows[i]["HORA"]));
					string str6 = this.Read.Tables[0].Rows[i]["PRODUCTO"].ToString();
					double num7 = Convert.ToDouble(this.Read.Tables[0].Rows[i]["CANTIDAD"]);
					//buscar el precio o lo hago al momento de buscar los datos 
					double num8 = Convert.ToDouble(this.Read.Tables[0].Rows[i]["PRECIO"]);
					double num9 = num8 * num7; //subtotal
					double num10 = num8 * num7;
					SqlCommand sqlCommand = new SqlCommand("SELECT top 1 * from MA_CODIGOS \nWHERE c_codigo = @c_codigo", function.connectionString)
					{
						Transaction = sqlTransaction
					};
					sqlCommand.Parameters.AddWithValue("@c_codigo", str5);
					SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
					if (sqlDataReader.HasRows)
					{
						sqlDataReader.Read();
						str1 = Convert.ToString(sqlDataReader["c_codigo"]);
						str2 = Convert.ToString(sqlDataReader["c_codnasa"]);
					} else
                    {
                        sqlDataReader.Close();
                        throw new Exception("El Producto / PLU con codigo [" + str5 + "][" + str6 + "] no está creado en el sistema.");
                    }
					sqlDataReader.Close();
					SqlCommand sqlCommand1 = new SqlCommand("select * from MA_PRODUCTOS   \nwhere C_CODIGO= @c_codigo", function.connectionString)
					{
						Transaction = sqlTransaction
					};
					sqlCommand1.Parameters.AddWithValue("@c_codigo", str2);
					SqlDataReader sqlDataReader1 = sqlCommand1.ExecuteReader();
					if (sqlDataReader1.HasRows)
					{
						sqlDataReader1.Read();
						num3 = Convert.ToDouble(sqlDataReader1["n_impuesto1"]);
						str3 = sqlDataReader1["n_costoact"].ToString();
						num4 = num10 * (num3 / 100);
						num5 = num10 + num4;
					}
					sqlDataReader1.Close();
					num += num4;
					num1 += num10;
					num2 += num5;
					try
					{
						SqlCommand sqlCommand2 = new SqlCommand("insert into TR_TRANSACCION_TEMP (CODIGO, DESCRIPCION, CANTIDAD, PRECIO, PRECIO_1, SUBTOTAL, IMPUESTO, TOTAL, PRECIOREAL, IMPUESTO1, IMPUESTO2, IMPUESTO3, COD_PRINCIPAL, N_COSTO, ID_HIJO) values (@PLU, @ITEM, @WEIGHT, @PRICE, @PRICE, @SUBTOTAL,@IMPUESTO, @TOTAL, @PRICE, @n_impuesto1, '0', '0',@COD_PRINCIPAL, @N_COSTO, @ID_HIJO)", function.connectionString)
						{
							Transaction = sqlTransaction
						};
						sqlCommand2.Parameters.AddWithValue("@VENDOR", strprov); // num6);
						sqlCommand2.Parameters.AddWithValue("@NUME", str4);
						sqlCommand2.Parameters.AddWithValue("@FECHA_HORA", dateTime);
						sqlCommand2.Parameters.AddWithValue("@ITEM", str6);
						sqlCommand2.Parameters.AddWithValue("@WEIGHT", num7);
						sqlCommand2.Parameters.AddWithValue("@PRICE", num8);
						sqlCommand2.Parameters.AddWithValue("@PRICExWEIGHT", num9);
						sqlCommand2.Parameters.AddWithValue("@PLU", str5);
						sqlCommand2.Parameters.AddWithValue("@SUBTOTAL", num10);
						sqlCommand2.Parameters.AddWithValue("@COD_PRINCIPAL", str2);
						sqlCommand2.Parameters.AddWithValue("@IMPUESTO", num4);
						sqlCommand2.Parameters.AddWithValue("@TOTAL", num5);
						sqlCommand2.Parameters.AddWithValue("@ID_HIJO", this.Gid_hijo);
						sqlCommand2.Parameters.AddWithValue("@N_COSTO", str3);
						sqlCommand2.Parameters.AddWithValue("@n_impuesto1", num3);
						sqlCommand2.ExecuteNonQuery();
					}
					catch (Exception exception)
					{
						Console.WriteLine(exception.Message);
						sqlTransaction.Rollback();
						MessageBox.Show("Error al llenar la tabla TR_TRANSACCION_TEMP");
						Environment.Exit(1);
					}
				}
			}
			catch (Exception exception1)
			{
				Console.WriteLine(exception1.Message);
				sqlTransaction.Rollback();
                MessageBox.Show("Error al cargar productos\n\n" + exception1.Message);
                return;
			}

			try
			{
				bool flag = false;
				SqlCommand sqlCommand3 = new SqlCommand("select TOP 1 * from MA_TRANSACCION_TEMP where ID_PADRE = @Cod", function.connectionString)
				{
					Transaction = sqlTransaction
				};
                sqlCommand3.Parameters.AddWithValue("@Cod", this.Gid_hijo);
				SqlDataReader sqlDataReader2 = sqlCommand3.ExecuteReader();
				if (sqlDataReader2.HasRows)
				{
					sqlDataReader2.Read();
					str = sqlDataReader2["ID_PADRE"].ToString();
                    flag = true;
                }
                else
                {
                    str = this.Gid_hijo;
                }
				if (!flag)
				{
					sqlDataReader2.Close();
					string str7 = DateTime.Now.ToString("yyyyMMdd hh:mm:ss");
					SqlCommand sqlCommand4 = new SqlCommand("insert into MA_TRANSACCION_TEMP (ID_PADRE, ACTIVA, CODIGO, NOMBRE, SUBTOTAL, IMPUESTO, TOTAL, TPRECIO, LIMITE, BIMPUESTO, RIF, FS_FECHA_HORA, cu_direccion_cliente, BS_IMPRESA, BS_CONTRIBUYENTE, cs_documento_rel) values(@id_hijo, @ACTIVA, @codigo, @nombre, '1', '2',  '3', @tprecio, '0', @bimpuesto, @rif, @datetime, @cu_direccion_cliente, '0', @bs_contribuyente, '')", function.connectionString)
					{
						Transaction = sqlTransaction
					};
					str = this.Gid_hijo;
					sqlCommand4.Parameters.AddWithValue("@datetime", str7);
					sqlCommand4.Parameters.AddWithValue("@id_hijo", str);
					sqlCommand4.Parameters.AddWithValue("@ACTIVA", 1);
					sqlCommand4.Parameters.AddWithValue("@codigo", this.Gcodigo);
					sqlCommand4.Parameters.AddWithValue("@nombre", this.Gnombre);
					sqlCommand4.Parameters.AddWithValue("@tprecio", this.Gtprecio);
					sqlCommand4.Parameters.AddWithValue("@rif", this.Grif);
					sqlCommand4.Parameters.AddWithValue("@cu_direccion_cliente", this.Gcu_direccion_cliente);
					sqlCommand4.Parameters.AddWithValue("@bs_contribuyente", this.Gbs_contribuyente);
					sqlCommand4.Parameters.AddWithValue("@bimpuesto", this.Gbimpuesto);
					sqlCommand4.ExecuteNonQuery();
					flag = true;
				}
				sqlDataReader2.Close();
			}
			catch (Exception exception2)
			{
				Console.WriteLine(exception2.Message);
				sqlTransaction.Rollback();
				MessageBox.Show("Error al llenar la tabla MA_TRANSACCION_TEMP");
				Environment.Exit(1);
			}
			try
			{
				try
				{
					if ((num2 > 0 || num1 > 0 ? true : num > 0))
					{
						SqlCommand sqlCommand5 = new SqlCommand(" UPDATE MA_TRANSACCION_TEMP SET IMPUESTO = @sumImpuesto, TOTAL = @sumTotal, SUBTOTAL = @sumSubtotal WHERE ID_PADRE = @ID_PADRE", function.connectionString)
						{
							Transaction = sqlTransaction
						};
						sqlCommand5.Parameters.AddWithValue("@sumTotal", num2);
						sqlCommand5.Parameters.AddWithValue("@sumSubtotal", num1);
						sqlCommand5.Parameters.AddWithValue("@sumImpuesto", num);
						sqlCommand5.Parameters.AddWithValue("@ID_PADRE", str);
						sqlCommand5.ExecuteNonQuery();
					}
					sqlTransaction.Commit();
					MessageBox.Show("Proceso terminado");
					Environment.Exit(1);
				}
				catch (Exception exception3)
				{
					Console.WriteLine(exception3.Message);
					sqlTransaction.Rollback();
					MessageBox.Show("Error al totalizar");
					Environment.Exit(1);
				}
			}
			finally
			{
				function.connectionString.Close();
			}

			switch (Settings.Default.Modalidad)
			{
				case 1:

					break;

			}

            if (Settings.Default.Modalidad == 1)
			{

			}
			else
			{ 
			}
		}

		public string CtrlC(string Text)
		{
			Clipboard.Clear();
			Clipboard.SetText(Text);
			return Text;
		}

		private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Functions.LoadSettings();
			string[] localDBSQLServerName = new string[] { "Server=", Settings.Default.LocalDB_SQLServerName, ";Database=", Settings.Default.LocalDB_SQLDBName, ";User ID=", Settings.Default.LocalDB_SQLUser, ";Password=", Settings.Default.LocalDB_SQLPass };
			Functions.ConnectionData = string.Concat(localDBSQLServerName);
			this.llenarCampos();

			if (this.param)
			{
				MessageBox.Show("Falta algún parametro");
				Environment.Exit(0);
			}

            base.ActiveControl = this.txtBoxNumero;

            switch (Settings.Default.Modalidad)
			{
				case 0:

                    this.Conn.ConnectionString = string.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source = ", Settings.Default.LocalMDB);
                    this.Conn.Open();

                    if (this.Conn.State == ConnectionState.Open)
                    {
                        this.txtBoxNumero.KeyUp += new KeyEventHandler(this.TextBoxKeyUp);
                    }
                    break;
				case 1:
					this.CASDBConnection.ConnectionString = "Server=" + Settings.Default.LocalDB_SQLCASServerName  +";Database="+ Settings.Default.LocalDB_SQLCASDBName  + 
						";User ID="+ Settings.Default.LocalDB_SQLCASUser  + ";Password=" + Settings.Default.LocalDB_SQLCASPass ;
					this.CASDBConnection.Open();

					if (this.CASDBConnection.State == ConnectionState.Open)
					{
                        this.txtBoxNumero.KeyUp += new KeyEventHandler(this.TextBoxKeyUp);
                    }

                    break;
			}


			this.SetFormLocation();
		}

		public string GetLines(long HowMany = 1L)
		{
			string empty = string.Empty;
			long howMany = HowMany;
			for (int i = 1; (long)i <= howMany; i++)
			{
				empty = string.Concat(empty, char.ConvertFromUtf32(13), char.ConvertFromUtf32(10));
			}
			return empty;
		}

		public string GetTab(long HowMany = 1L)
		{
			string empty = string.Empty;
			long howMany = HowMany;
			for (int i = 1; (long)i <= howMany; i++)
			{
				empty = string.Concat(empty, char.ConvertFromUtf32(Convert.ToInt32(Keys.Tab)));
			}
			return empty;
		}

		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnImportar = new System.Windows.Forms.Button();
            this.lblPesada = new System.Windows.Forms.Label();
            this.txtBoxNumero = new System.Windows.Forms.TextBox();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImportar
            // 
            this.btnImportar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnImportar.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportar.Location = new System.Drawing.Point(319, 124);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(285, 39);
            this.btnImportar.TabIndex = 0;
            this.btnImportar.Text = "Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblPesada
            // 
            this.lblPesada.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPesada.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPesada.Location = new System.Drawing.Point(371, 28);
            this.lblPesada.Name = "lblPesada";
            this.lblPesada.Size = new System.Drawing.Size(179, 25);
            this.lblPesada.TabIndex = 1;
            this.lblPesada.Text = "Introduzca el Ticket";
            this.lblPesada.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtBoxNumero
            // 
            this.txtBoxNumero.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxNumero.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxNumero.Location = new System.Drawing.Point(319, 67);
            this.txtBoxNumero.Name = "txtBoxNumero";
            this.txtBoxNumero.Size = new System.Drawing.Size(283, 33);
            this.txtBoxNumero.TabIndex = 2;
            this.txtBoxNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBoxNumero.WordWrap = false;
            this.txtBoxNumero.TextChanged += new System.EventHandler(this.txtBoxNumero_TextChanged);
            // 
            // dataGrid
            // 
            this.dataGrid.AllowUserToAddRows = false;
            this.dataGrid.AllowUserToDeleteRows = false;
            this.dataGrid.AllowUserToOrderColumns = true;
            this.dataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Location = new System.Drawing.Point(21, 185);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGrid.Size = new System.Drawing.Size(963, 520);
            this.dataGrid.TabIndex = 3;
            this.dataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_CellContentClick);
            // 
            // ReadDB
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.txtBoxNumero);
            this.Controls.Add(this.lblPesada);
            this.Controls.Add(this.btnImportar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadDB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		public bool isDBNull(dynamic pValue)
		{
			return (bool)Convert.IsDBNull(pValue);
		}

		public dynamic isDBNull(dynamic pValue, dynamic pDefaultValueReturned)
		{
			return (!this.isDBNull(pValue) ? pValue : pDefaultValueReturned);
		}

		private void label1_Click(object sender, EventArgs e)
		{
		}

		public void llenarCampos()
		{
			Program.Params.CurrentParameter = "codigo";
			if (Program.Params.ParameterExists)
			{
				this.Gcodigo = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "nombre";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gnombre = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "tprecio";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gtprecio = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "rif";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Grif = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "cu_direccion_cliente";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gcu_direccion_cliente = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "bs_contribuyente";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gbs_contribuyente = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "id_hijo";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gid_hijo = Program.Params.ParameterValue;
			}
			Program.Params.CurrentParameter = "bimpuesto";
			if (!Program.Params.ParameterExists)
			{
				this.param = true;
			}
			else
			{
				this.Gbimpuesto = Program.Params.ParameterValue;
			}
			//agregamos los parametros necesarios para la segunda modalidad 
			Program.Params.CurrentParameter = "nFactura";
			if (Program.Params.ParameterExists)
			{
				this.GNumeroFactura = Program.Params.ParameterValue;
			}
			//Program.Params.CurrentParameter = "modalidad";
			//if (!Program.Params.ParameterExists)
			//{
			//	this.param = true;
			//}
			//else
			//{
			//	this.GModalidad = Program.Params.ParameterValue;
			//}
			//se comenta el codigo de arriba para no alterar el proceso ya que la aplicacion no se hara por esta via sino por setup
		}

		public string PrintDisposableRecordset(ref Recordset pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A")
		{
			string str = this.PrintRecordset(this.RsToDataTable(ref pRs, null), pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, 0);
			return str;
		}

		public string PrintDisposableRecordset(ref OleDbDataReader pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			string str = this.PrintRecordset(this.RsToDataTable(ref pRs, null), pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel);
			return str;
		}

		public string PrintDisposableRecordset(ref SqlDataReader pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A")
		{
			string str = this.PrintRecordset(this.RsToDataTable(ref pRs, null), pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, 0);
			return str;
		}

		public string PrintRecord(ref OleDbDataReader pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A")
		{
			OleDbCommand oleDbCommand = null;
			string str = this.PrintRecordset(ref pRs, ref oleDbCommand, pRowNumAlias, true, EncloseHeaders, LineBeforeData, pOnValueError, 0);
			return str;
		}

		public string PrintRecord(ref SqlDataReader pRs, ref SqlCommand pRsSource, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A")
		{
			string str = this.PrintRecordset(ref pRs, ref pRsSource, "", true, EncloseHeaders, LineBeforeData, pOnValueError, 0);
			return str;
		}

		public string PrintRecord(DataRow pRecord, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			int i;
			string empty;
			string item = string.Empty;
			dynamic obj = null;
			List<int> nums = new List<int>();
			int num = 0;
			try
			{
				try
				{
					for (i = 0; i <= pRecord.Table.Columns.Count - 1; i++)
					{
						num = i;
						object obj1 = item;
						object[] objArray = new object[] { obj1, "$(mCol[", num, "])" };
						item = string.Concat(objArray);
						nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), pRecord.Table.Columns[num].ColumnName, (EncloseHeaders ? "]" : string.Empty)).Length);
					}
					if (LineBeforeData)
					{
						item = string.Concat(item, this.GetLines((long)1));
					}
					item = string.Concat(item, this.GetLines((long)1));
					for (i = 0; i <= pRecord.Table.Columns.Count - 1; i++)
					{
						num = i;
						obj = this.isDBNull((dynamic)this.SafeItem(pRecord, pRecord.Table.Columns[i].ColumnName, pOnValueError), "NULL");
						if (Convert.ToString(obj).Length > nums[num])
						{
							nums[num] = (int)Convert.ToString(obj).Length;
						}
						item = item.Replace(string.Concat("$(mCol[", num, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), pRecord.Table.Columns[i].ColumnName, (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num], ' '), this.GetTab((long)1)));
						item = (string)(item + Convert.ToString(obj).PadRight(nums[num], ' ') + this.GetTab((long)1));
					}
					empty = item;
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
					empty = string.Empty;
				}
			}
			finally
			{
			}
			return empty;
		}

		public string PrintRecord(ref Recordset pRs, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A")
		{
			string str = this.PrintRecordset(ref pRs, "", true, EncloseHeaders, LineBeforeData, pOnValueError, 0);
			return str;
		}

		public string PrintRecordset(ref OleDbDataReader pRs, ref OleDbCommand pRsSource, string pRowNumAlias = "", bool OnlyCurrentRow = false, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			int i;
			string empty;
			string tab = string.Empty;
			dynamic obj = null;
			OleDbDataReader oleDbDataReader = null;
			List<int> nums = new List<int>();
			int num = 0;
			try
			{
				try
				{
					if (pRs == null)
					{
						pRs = pRsSource.ExecuteReader();
					}
					if (OnlyCurrentRow)
					{
						for (i = 0; i <= pRs.FieldCount - 1; i++)
						{
							num = i;
							object obj1 = tab;
							object[] objArray = new object[] { obj1, this.GetTab((long)NestLevel), "$(mCol[", num, "])" };
							tab = string.Concat(objArray);
							nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.GetName(i), (EncloseHeaders ? "]" : string.Empty)).Length);
						}
						if (pRs.Read())
						{
							for (i = 0; i <= pRs.FieldCount - 1; i++)
							{
								obj = this.isDBNull((dynamic)this.SafeItem(pRs, pRs.GetName(i), pOnValueError), "NULL");
								num = i;
								if (!(obj is OleDbDataReader))
								{
									if (Convert.ToString(obj).Length > nums[num])
									{
										nums[num] = (int)Convert.ToString(obj).Length;
									}
								}
							}
						}
						for (i = pRs.FieldCount - 1; i >= 0; i--)
						{
							num = i;
							tab = tab.Replace(string.Concat("$(mCol[", num, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.GetName(i), (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num], ' '), this.GetTab((long)1)));
						}
						if (pRs.Read())
						{
							tab = string.Concat(tab, this.GetLines((long)1));
							for (i = 0; i <= pRs.FieldCount - 1; i++)
							{
								obj = this.isDBNull((dynamic)this.SafeItem(pRs, pRs.GetName(i), pOnValueError), "NULL");
								num = i;
								if (obj is OleDbDataReader)
								{
									oleDbDataReader = (OleDbDataReader)obj;
									tab = string.Concat(tab, this.GetLines((long)1), this.PrintRecordset(ref oleDbDataReader, ref pRsSource, pRowNumAlias, false, EncloseHeaders, LineBeforeData, pOnValueError, Convert.ToByte(NestLevel + 1)));
								}
								else
								{
									tab = (string)(tab + this.GetTab((long)NestLevel) + Convert.ToString(obj).PadRight(nums[num], ' ') + this.GetTab((long)1));
								}
							}
						}
						empty = tab;
					}
					else
					{
						empty = this.PrintDisposableRecordset(ref pRs, pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel);
					}
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
					empty = string.Empty;
				}
			}
			finally
			{
				if (!OnlyCurrentRow & NestLevel == 0)
				{
					pRs.Close();
					pRs = pRsSource.ExecuteReader();
				}
			}
			return empty;
		}

		public string PrintRecordset(ref SqlDataReader pRs, ref SqlCommand pRsSource, string pRowNumAlias = "", bool OnlyCurrentRow = false, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			int i;
			DataRow row = null;
			string empty;
			object obj;
			object[] objArray;
			string tab = string.Empty;
			bool flag = false;
			string str = string.Empty;
			long num = (long)0;
			dynamic obj1 = null;
			List<int> nums = new List<int>();
			int num1 = 0;
			bool hasRows = false;
			DataTable dataTable = null;
			try
			{
				try
				{
					dataTable = new DataTable();
					if (pRs == null)
					{
						pRs = pRsSource.ExecuteReader();
					}
					hasRows = pRs.HasRows;
					if (!OnlyCurrentRow)
					{
						dataTable.Load(pRs);
					}
					else
					{
						pRowNumAlias = string.Empty;
					}
					flag = pRowNumAlias != string.Empty;
					if (flag)
					{
						str = string.Concat((EncloseHeaders ? "[" : string.Empty), pRowNumAlias, (EncloseHeaders ? "]" : string.Empty));
						num = (long)0;
						tab = string.Concat(tab, this.GetTab((long)NestLevel), "$(mCol[0])");
						nums.Add(str.Length);
					}
					if (!OnlyCurrentRow)
					{
						for (i = 0; i <= dataTable.Columns.Count - 1; i++)
						{
							num1 = (flag ? i + 1 : i);
							obj = tab;
							objArray = new object[] { obj, null, null, null, null };
							objArray[1] = (!flag ? this.GetTab((long)NestLevel) : string.Empty);
							objArray[2] = "$(mCol[";
							objArray[3] = num1;
							objArray[4] = "])";
							tab = string.Concat(objArray);
							nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), dataTable.Columns[i].ColumnName, (EncloseHeaders ? "]" : string.Empty)).Length);
						}
					}
					else
					{
						for (i = 0; i <= pRs.FieldCount - 1; i++)
						{
							num1 = (flag ? i + 1 : i);
							obj = tab;
							objArray = new object[] { obj, null, null, null, null };
							objArray[1] = (!flag ? this.GetTab((long)NestLevel) : string.Empty);
							objArray[2] = "$(mCol[";
							objArray[3] = num1;
							objArray[4] = "])";
							tab = string.Concat(objArray);
							nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.GetName(i), (EncloseHeaders ? "]" : string.Empty)).Length);
						}
					}
					if (LineBeforeData)
					{
						tab = string.Concat(tab, this.GetLines((long)1));
					}
					num = (long)0;
					if (!OnlyCurrentRow)
					{
						foreach (DataRow tmprow in dataTable.Rows)
						{
							if (flag)
							{
								obj1 = num;
								if (Convert.ToString(obj1).Length > nums[0])
								{
									nums[0] = (int)Convert.ToString(obj1).Length;
								}
							}
							for (i = 0; i <= dataTable.Columns.Count - 1; i++)
							{
                                obj1 = this.isDBNull((dynamic)this.SafeItem(tmprow, dataTable.Columns[i].ColumnName, pOnValueError), "NULL");
								num1 = (flag ? i + 1 : i);
								if (Convert.ToString(obj1).Length > nums[num1])
								{
									nums[num1] = (int)Convert.ToString(obj1).Length;
								}
							}
							num += (long)1;
						}
					}
					else
					{
						while (pRs.Read())
						{
							if (flag)
							{
								obj1 = num;
								if (Convert.ToString(obj1).Length > nums[0])
								{
									nums[0] = (int)Convert.ToString(obj1).Length;
								}
							}
							for (i = 0; i <= pRs.FieldCount - 1; i++)
							{
								obj1 = this.isDBNull((dynamic)this.SafeItem(pRs, pRs.GetName(i), pOnValueError), "NULL");
								num1 = (flag ? i + 1 : i);
								if (Convert.ToString(obj1).Length > nums[num1])
								{
									nums[num1] = (int)Convert.ToString(obj1).Length;
								}
							}
							num += (long)1;
							if (OnlyCurrentRow)
							{
								break;
							}
						}
					}
					if (flag)
					{
						tab = tab.Replace("$(mCol[0])", string.Concat(str.PadRight(nums[0], ' '), this.GetTab((long)1)));
					}
					if (!OnlyCurrentRow)
					{
						for (i = dataTable.Columns.Count - 1; i >= 0; i--)
						{
							num1 = (flag ? i + 1 : i);
							tab = tab.Replace(string.Concat("$(mCol[", num1, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), dataTable.Columns[i].ColumnName, (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num1], ' '), this.GetTab((long)1)));
						}
					}
					else
					{
						for (i = pRs.FieldCount - 1; i >= 0; i--)
						{
							num1 = (flag ? i + 1 : i);
							tab = tab.Replace(string.Concat("$(mCol[", num1, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.GetName(i), (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num1], ' '), this.GetTab((long)1)));
						}
					}
					num = (long)0;
					if (!OnlyCurrentRow)
					{
						foreach (DataRow dataRow in dataTable.Rows)
						{
							tab = string.Concat(tab, this.GetLines((long)1));
							if (flag)
							{
								obj1 = num;
								tab = (string)(tab + this.GetTab((long)NestLevel) + Convert.ToString(obj1).PadRight(nums[0], ' ') + this.GetTab((long)1));
							}
							for (i = 0; i <= dataTable.Columns.Count - 1; i++)
							{
								obj1 = this.isDBNull((dynamic)this.SafeItem(dataRow, dataTable.Columns[i].ColumnName, pOnValueError), "NULL");
								num1 = (flag ? i + 1 : i);
								tab = (string)(tab + (!flag ? this.GetTab((long)NestLevel) : string.Empty) + Convert.ToString(obj1).PadRight(nums[num1], ' ') + this.GetTab((long)1));
							}
							num += (long)1;
						}
					}
					else
					{
						while (pRs.Read())
						{
							tab = string.Concat(tab, this.GetLines((long)1));
							if (flag)
							{
								obj1 = num;
								tab = (string)(tab + this.GetTab((long)NestLevel) + Convert.ToString(obj1).PadRight(nums[0], ' ') + this.GetTab((long)1));
							}
							for (i = 0; i <= pRs.FieldCount - 1; i++)
							{
								obj1 = this.isDBNull((dynamic)this.SafeItem(pRs, pRs.GetName(i), pOnValueError), "NULL");
								num1 = (flag ? i + 1 : i);
								tab = (string)(tab + (!flag ? this.GetTab((long)NestLevel) : string.Empty) + Convert.ToString(obj1).PadRight(nums[num1], ' ') + this.GetTab((long)1));
							}
							num += (long)1;
							if (OnlyCurrentRow)
							{
								break;
							}
						}
					}
					if (hasRows & !OnlyCurrentRow)
					{
						pRs.Close();
						pRs = pRsSource.ExecuteReader();
					}
					empty = tab;
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
					empty = string.Empty;
				}
			}
			finally
			{
				if (hasRows & !OnlyCurrentRow)
				{
					pRs.Close();
					pRs = pRsSource.ExecuteReader();
				}
			}
			return empty;
		}

		public string PrintRecordset(DataTable pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			int i;
			DataRow row = null;
			string empty;
			string tab = string.Empty;
			bool flag = false;
			string str = string.Empty;
			long num = (long)0;
			dynamic obj = null;
			DataTable dataTable = null;
			List<int> nums = new List<int>();
			int num1 = 0;
			bool count = false;
			try
			{
				count = pRs.Rows.Count > 0;
				flag = pRowNumAlias != string.Empty;
				if (flag)
				{
					str = string.Concat((EncloseHeaders ? "[" : string.Empty), pRowNumAlias, (EncloseHeaders ? "]" : string.Empty));
					num = (long)0;
					tab = string.Concat(tab, this.GetTab((long)NestLevel), "$(mCol[0])");
					nums.Add(str.Length);
				}
				for (i = 0; i <= pRs.Columns.Count - 1; i++)
				{
					num1 = (flag ? i + 1 : i);
					object obj1 = tab;
					object[] objArray = new object[] { obj1, null, null, null, null };
					objArray[1] = (!flag ? this.GetTab((long)NestLevel) : string.Empty);
					objArray[2] = "$(mCol[";
					objArray[3] = num1;
					objArray[4] = "])";
					tab = string.Concat(objArray);
					nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.Columns[i].ColumnName, (EncloseHeaders ? "]" : string.Empty)).Length);
				}
				if (LineBeforeData)
				{
					tab = string.Concat(tab, this.GetLines((long)1));
				}
				num = (long)0;
                foreach (DataRow tmprow in pRs.Rows)
				{
					if (flag)
					{
						obj = num;
						if (Convert.ToString(obj).Length > nums[0])
						{
							nums[0] = (int)Convert.ToString(obj).Length;
						}
					}
					for (i = 0; i <= pRs.Columns.Count - 1; i++)
					{
                        obj = this.isDBNull((dynamic)this.SafeItem(tmprow, pRs.Columns[i].ColumnName, pOnValueError), "NULL");
						num1 = (flag ? i + 1 : i);
						if (!(obj is DataTable))
						{
							if (Convert.ToString(obj).Length > nums[num1])
							{
								nums[num1] = (int)Convert.ToString(obj).Length;
							}
						}
					}
					num += (long)1;
				}
				if (flag)
				{
					tab = tab.Replace("$(mCol[0])", string.Concat(str.PadRight(nums[0], ' '), this.GetTab((long)1)));
				}
				for (i = pRs.Columns.Count - 1; i >= 0; i--)
				{
					num1 = (flag ? i + 1 : i);
					tab = tab.Replace(string.Concat("$(mCol[", num1, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.Columns[i].ColumnName, (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num1], ' '), this.GetTab((long)1)));
				}
				num = (long)0;
				foreach (DataRow dataRow in pRs.Rows)
				{
					tab = string.Concat(tab, this.GetLines((long)1));
					if (flag)
					{
						obj = num;
						tab = (string)(tab + this.GetTab((long)NestLevel) + Convert.ToString(obj).PadRight(nums[0], ' ') + this.GetTab((long)1));
					}
					for (i = 0; i <= pRs.Columns.Count - 1; i++)
					{
						obj = this.isDBNull((dynamic)this.SafeItem(dataRow, pRs.Columns[i].ColumnName, pOnValueError), "NULL");
						num1 = (flag ? i + 1 : i);
						if (obj is DataTable)
						{
							dataTable = (DataTable)obj;
							tab = string.Concat(tab, this.GetLines((long)1), this.PrintRecordset(dataTable, pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, Convert.ToByte(NestLevel + 1)));
						}
						else
						{
							tab = (string)(tab + (!flag ? this.GetTab((long)NestLevel) : string.Empty) + Convert.ToString(obj).PadRight(nums[num1], ' ') + this.GetTab((long)1));
						}
					}
					num += (long)1;
				}
				empty = tab;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
				empty = string.Empty;
			}
			return empty;
		}

		public string PrintRecordset(DataSet pRs, string pRowNumAlias = "", bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			string empty = string.Empty;
			foreach (DataTable table in pRs.Tables)
			{
				string str = empty;
				string[] dataSetName = new string[] { str, "[", pRs.DataSetName, "].[", table.TableName, "]" };
				empty = string.Concat(dataSetName);
				empty = string.Concat(empty, this.GetLines((long)2));
				empty = string.Concat(empty, this.PrintRecordset(table, pRowNumAlias, EncloseHeaders, LineBeforeData, pOnValueError, 0));
				empty = string.Concat(empty, this.GetLines((long)2));
			}
			return empty;
		}

		public string PrintRecordset(ref Recordset pRs, string pRowNumAlias = "", bool OnlyCurrentRow = false, bool EncloseHeaders = true, bool LineBeforeData = false, string pOnValueError = "Error|N/A", byte NestLevel = 0)
		{
			int i;
			string empty;
			string tab = string.Empty;
			bool flag = false;
			string str = string.Empty;
			long num = (long)0;
			dynamic obj = null;
			dynamic obj1 = -1;
			Recordset variable = null;
			Recordset variable1 = null;
			List<int> nums = new List<int>();
			int num1 = 0;
			bool eOF = false;
			try
			{
				try
				{
					eOF = !(pRs.EOF & pRs.BOF);
					if (OnlyCurrentRow)
					{
						pRowNumAlias = string.Empty;
					}
					flag = pRowNumAlias != string.Empty;
					if (flag)
					{
						str = string.Concat((EncloseHeaders ? "[" : string.Empty), pRowNumAlias, (EncloseHeaders ? "]" : string.Empty));
						num = (long)0;
						tab = string.Concat(tab, this.GetTab((long)NestLevel), "$(mCol[0])");
						nums.Add(str.Length);
					}
					for (i = 0; i <= pRs.Fields.Count - 1; i++)
					{
						num1 = (flag ? i + 1 : i);
						object obj2 = tab;
						object[] objArray = new object[] { obj2, null, null, null, null };
						objArray[1] = (!flag ? this.GetTab((long)NestLevel) : string.Empty);
						objArray[2] = "$(mCol[";
						objArray[3] = num1;
						objArray[4] = "])";
						tab = string.Concat(objArray);
						nums.Add(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.Fields[i].Name, (EncloseHeaders ? "]" : string.Empty)).Length);
					}
					if (LineBeforeData)
					{
						tab = string.Concat(tab, this.GetLines((long)1));
					}
					if (!(eOF & !OnlyCurrentRow))
					{
						variable1 = pRs;
					}
					else
					{
						obj1 = this.SafeProp(pRs, "Bookmark", -1);
						if (pRs.CursorType == CursorTypeEnum.adOpenForwardOnly)
						{
							pRs.Requery(-1);
						}
						else
						{
							pRs.MoveFirst();
						}
						if (obj1 != -1)
						{
							variable1 = pRs.Clone(LockTypeEnum.adLockReadOnly);
						}
					}
					num = (long)0;
					while (!pRs.EOF)
					{
						if (flag)
						{
							obj = num;
							if (Convert.ToString(obj).Length > nums[0])
							{
								nums[0] = (int)Convert.ToString(obj).Length;
							}
						}
						for (i = 0; i <= pRs.Fields.Count - 1; i++)
						{
							obj = this.isDBNull(((dynamic)this.SafeItem(pRs.Fields, pRs.Fields[i].Name, pOnValueError)).Value, "NULL");
							num1 = (flag ? i + 1 : i);
							if (!(obj is Recordset))
							{
								if (Convert.ToString(obj).Length > nums[num1])
								{
									nums[num1] = (int)Convert.ToString(obj).Length;
								}
							}
						}
						num += (long)1;
						if (!OnlyCurrentRow)
						{
							pRs.MoveNext();
						}
						else
						{
							break;
						}
					}
					if (flag)
					{
						tab = tab.Replace("$(mCol[0])", string.Concat(str.PadRight(nums[0], ' '), this.GetTab((long)1)));
					}
					for (i = pRs.Fields.Count - 1; i >= 0; i--)
					{
						num1 = (flag ? i + 1 : i);
						tab = tab.Replace(string.Concat("$(mCol[", num1, "])"), string.Concat(string.Concat((EncloseHeaders ? "[" : string.Empty), pRs.Fields[i].Name, (EncloseHeaders ? "]" : string.Empty)).PadRight(nums[num1], ' '), this.GetTab((long)1)));
					}
					if (obj1 == -1 & !OnlyCurrentRow)
					{
						variable1 = pRs;
						if (eOF)
						{
							if (variable1.CursorType == CursorTypeEnum.adOpenForwardOnly)
							{
								variable1.Requery(-1);
							}
							else
							{
								variable1.MoveFirst();
							}
						}
					}
					num = (long)0;
					while (!variable1.EOF)
					{
						tab = string.Concat(tab, this.GetLines((long)1));
						if (flag)
						{
							obj = num;
							tab = (string)(tab + this.GetTab((long)NestLevel) + Convert.ToString(obj).PadRight(nums[0], ' ') + this.GetTab((long)1));
						}
						for (i = 0; i <= variable1.Fields.Count - 1; i++)
						{
							obj = this.isDBNull(((dynamic)this.SafeItem(variable1.Fields, variable1.Fields[i].Name, pOnValueError)).Value, "NULL");
							num1 = (flag ? i + 1 : i);
							if (obj is Recordset)
							{
								variable = (Recordset)obj;
								tab = string.Concat(tab, this.GetLines((long)1), this.PrintRecordset(ref variable, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, Convert.ToByte(NestLevel + 1)));
							}
							else
							{
								tab = (string)(tab + (!flag ? this.GetTab((long)NestLevel) : string.Empty) + Convert.ToString(obj).Replace(this.GetLines((long)1), " ").PadRight(nums[num1], ' ') + this.GetTab((long)1));
							}
						}
						num += (long)1;
						if (!OnlyCurrentRow)
						{
							variable1.MoveNext();
						}
						else
						{
							break;
						}
					}
					empty = tab;
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
					empty = string.Empty;
				}
			}
			finally
			{
				try
				{
					if (eOF & !OnlyCurrentRow)
					{
						if (obj1 != -1)
						{
							if (pRs.CursorType != CursorTypeEnum.adOpenForwardOnly)
							{
								this.SafePropAssign(pRs, "Bookmark", obj1);
							}
							else
							{
								pRs.Requery(-1);
								pRs.Move(obj1, 0);
							}
						}
						else if (variable1.CursorType != CursorTypeEnum.adOpenForwardOnly)
						{
							variable1.Requery(-1);
						}
						else
						{
							variable1.MoveFirst();
						}
					}
				}
				catch (Exception exception1)
				{
					Console.WriteLine(exception1.Message);
				}
			}
			return empty;
		}

		public DataTable RsToDataTable(ref Recordset pRs, string TableName = null)
		{
			DataTable dataTable = null;
			Recordset variable = null;
			dynamic value = null;
			dataTable = (TableName == null ? new DataTable() : new DataTable(TableName));
			List<object> objs = new List<object>();
			foreach (Field field in pRs.Fields)
			{
				dataTable.Columns.Add(field.Name, typeof(object));
				dataTable.Columns[field.Name].AllowDBNull = true;
				objs.Add(null);
			}
			while (!pRs.EOF)
			{
				DataRow dataRow = dataTable.NewRow();
				object[] array = objs.ToArray();
				foreach (DataColumn column in dataTable.Columns)
				{
					value = pRs.Fields[column.Ordinal].Value;
					if (value is Recordset)
					{
						variable = (Recordset)value;
						array[column.Ordinal] = this.RsToDataTable(ref variable, null);
					}
					else
					{
						array[column.Ordinal] = value;
					}
				}
				dataRow.ItemArray = array;
				dataTable.Rows.Add(dataRow);
				pRs.MoveNext();
			}
			return dataTable;
		}

		public DataTable RsToDataTable(ref OleDbDataReader pRs, string TableName = null)
		{
			DataTable dataTable = null;
			OleDbDataReader oleDbDataReader = null;
			dynamic value = null;
			dataTable = (TableName == null ? new DataTable() : new DataTable(TableName));
			List<object> objs = new List<object>();
			for (int i = 0; i <= pRs.FieldCount - 1; i++)
			{
				dataTable.Columns.Add(pRs.GetName(i), typeof(object));
				dataTable.Columns[pRs.GetName(i)].AllowDBNull = true;
				objs.Add(null);
			}
			while (pRs.Read())
			{
				DataRow dataRow = dataTable.NewRow();
				object[] array = objs.ToArray();
				foreach (DataColumn column in dataTable.Columns)
				{
					value = pRs.GetValue(column.Ordinal);
					if (value is OleDbDataReader)
					{
						oleDbDataReader = (OleDbDataReader)value;
						array[column.Ordinal] = this.RsToDataTable(ref oleDbDataReader, null);
					}
					else
					{
						array[column.Ordinal] = value;
					}
				}
				dataRow.ItemArray = array;
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		public DataTable RsToDataTable(ref SqlDataReader pRs, string TableName = null)
		{
			DataTable dataTable = null;
			SqlDataReader sqlDataReader = null;
			dynamic value = null;
			dataTable = (TableName == null ? new DataTable() : new DataTable(TableName));
			List<object> objs = new List<object>();
			for (int i = 0; i <= pRs.FieldCount - 1; i++)
			{
				dataTable.Columns.Add(pRs.GetName(i), typeof(object));
				dataTable.Columns[pRs.GetName(i)].AllowDBNull = true;
				objs.Add(null);
			}
			while (pRs.Read())
			{
				DataRow dataRow = dataTable.NewRow();
				object[] array = objs.ToArray();
				foreach (DataColumn column in dataTable.Columns)
				{
					value = pRs.GetValue(column.Ordinal);
					if (value is SqlDataReader)
					{
						sqlDataReader = (SqlDataReader)value;
						array[column.Ordinal] = this.RsToDataTable(ref sqlDataReader, null);
					}
					else
					{
						array[column.Ordinal] = value;
					}
				}
				dataRow.ItemArray = array;
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		public dynamic SafeItem(dynamic pObj, dynamic pItem, dynamic pDefaultValue)
		{
			object obj;
			try
			{
				obj = pObj[pItem];
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
				obj = pDefaultValue;
			}
			return obj;
		}

		public dynamic SafeProp(object pObj, string pProperty, dynamic pDefaultValue)
		{
			object obj;
			try
			{
				object value = ((IEnumerable<PropertyInfo>)pObj.GetType().GetProperties()).Single<PropertyInfo>((PropertyInfo Prop) => Prop.Name.Equals(pProperty, StringComparison.Ordinal)).GetValue(pObj);
				obj = value;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
				obj = pDefaultValue;
			}
			return obj;
		}

		public void SafePropAssign(object pObj, string pProperty, dynamic pValue)
		{
			try
			{
				PropertyInfo[] properties = pObj.GetType().GetProperties();
				((IEnumerable<PropertyInfo>)properties).Single<PropertyInfo>((PropertyInfo Prop) => Prop.Name.Equals(pProperty, StringComparison.Ordinal)).SetValue(pObj, pValue);
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
		}

		private void SetFormLocation()
		{
			try
			{
				Screen[] allScreens = Screen.AllScreens;
				Rectangle bounds = allScreens[0].Bounds;
				Point location = bounds.Location;
				bounds = allScreens[0].Bounds;
				System.Drawing.Size size = bounds.Size;
                //base.Left = location.X;
                //base.Top = location.Y;
                //base.Width = size.Width;
                //base.Height = size.Height - 250;
                //DataGridView width = this.dataGrid;
                //bounds = base.ClientRectangle;
                //width.Width = bounds.Width - 40;
                //DataGridView height = this.dataGrid;
                //bounds = base.ClientRectangle;
                //height.Height = bounds.Height - 150;
                //this.txtBoxNumero.TextAlign = HorizontalAlignment.Center;
				base.BringToFront();
			}
			catch (Exception exception)
			{
			}
		}

		private void TextBoxKeyUp(object sender, KeyEventArgs e)
		{
			if ((e.KeyCode == Keys.Return ? true : e.KeyCode == Keys.Return))
			{
				this.txtBoxNumero.SelectAll();
				this.txtBoxNumero.Focus();
				string text = this.txtBoxNumero.Text;
				string mFecha = DateTime.Now.ToString("yyyy/MM/dd");
				
				if (Settings.Default.LocalEscaner)
				{
					text = this.txtBoxNumero.Text.Substring(Settings.Default.LocalPosIni, Settings.Default.LocalPosFin);
				}
				//agregamos nuevo comportamiento para buscar los datos en la bd de balanzas cas

				switch (Settings.Default.Modalidad)
				{
					case 0:
                        OleDbCommand oleDbCommand = new OleDbCommand(string.Concat("SELECT NUME AS TICKET, PLU, VENDOR AS PROVEEDOR, DATE AS FECHA, HOUR AS HORA, ",
							"ITEM AS PRODUCTO, WEIGHT AS CANTIDAD, PRICE AS PRECIO, (PRICE * WEIGHT) as SUBTOTAL FROM TICKETS WHERE NUME = '", text, "'"))
                        {
                            Connection = this.Conn
                        };
                        OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter(oleDbCommand);
                        this.Read = new DataSet();

                        oleDbDataAdapter.Fill(this.Read, "TICKETS");
                        try
                        {
                            this.dataGrid.DataSource = this.Read;
                            this.dataGrid.DataMember = "TICKETS";
                            if (this.dataGrid.Rows.Count > 0)
                            {
                                this.dataGrid.Rows[this.dataGrid.Rows.Count - 1].Selected = true;
                                this.dataGrid.CurrentCell = this.dataGrid.Rows[this.dataGrid.Rows.Count - 1].Cells[1];
                            }
                            this.dataGrid.Refresh();
                            this.AjustarGrid();
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception.Message);
                        }
                        break;	

					case 1:
                        SqlConnection conn = new SqlConnection(this.CASDBConnection.ConnectionString);
						SqlCommand sqlcmd = new SqlCommand("SELECT n_Numero as TICKET, c_Codigo as PLU, c_CodVendedor as PROVEEDOR, CAST(F_FECHA as date) as FECHA, " +
                            " CAST(F_FECHA as time) as HORA, c_Descrip as PRODUCTO, n_cantidad as CANTIDAD, n_Precio as PRECIO, round((n_cantidad * n_precio),2) as SUBTOTAL " +
							" FROM PEDIDOS_CAS WHERE N_NUMERO = " + text + " AND CAST(F_FECHA as date) = cast('" + mFecha + "' as date)",conn);
						SqlDataAdapter da = new SqlDataAdapter();
						da.SelectCommand = sqlcmd;
						conn.Open();
                        this.Read = new DataSet();
                        //DataSet ds = new DataSet();
                        da.Fill(this.Read, "TICKETS");
                        //sqlcmd.Connection = this.CASDBConnection;
                        //SqlDataReader dr = sqlcmd.ExecuteReader();
                        //SqlDataAdapter da = new SqlDataAdapter(sqlcmd.CommandText , conn);	
                        //this.Read = new DataSet();
                        //DataTable dt= new DataTable();
                        try
                        {
							this.dataGrid.DataSource = this.Read;
                            this.dataGrid.DataMember = "TICKETS";
                            if (this.dataGrid.Rows.Count > 0)
                            {
                                this.dataGrid.Rows[this.dataGrid.Rows.Count - 1].Selected = true;
                                this.dataGrid.CurrentCell = this.dataGrid.Rows[this.dataGrid.Rows.Count - 1].Cells[1];
                            }
                            this.dataGrid.Refresh();
                            this.AjustarGrid();
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception.Message);
                        }
                        break;

				}		

			}
		}

		private void txtBoxNumero_TextChanged(object sender, EventArgs e)
		{
		}
	}
}
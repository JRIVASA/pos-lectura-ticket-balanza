using System;
using System.Collections;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Text;

namespace WindowsFormsApplication1
{
	internal class IniSettings
	{
		public IniSettings()
		{
		}

		public string Get(string Section, string Key, string CustomDefaultValue)
		{
			StringBuilder stringBuilder = new StringBuilder(10000);
			IniSettings.GetPrivateProfileString(Section, Key, CustomDefaultValue, stringBuilder, 10000, QuickSettings.FilePath);
			return stringBuilder.ToString();
		}

		public string Get(string Section, string Key, SettingsProperty Property)
		{
			StringBuilder stringBuilder = new StringBuilder(10000);
			IniSettings.GetPrivateProfileString(Section, Key, Property.DefaultValue.ToString(), stringBuilder, 10000, QuickSettings.FilePath);
			if (Property.Attributes["Section"] == null)
			{
				Property.Attributes.Add("Section", Section);
			}
			if (Property.Attributes["Key"] == null)
			{
				Property.Attributes.Add("Key", Key);
			}
			return stringBuilder.ToString();
		}

		public string Get(SettingsProperty Property)
		{
			StringBuilder stringBuilder = new StringBuilder(10000);
			IniSettings.GetPrivateProfileString(Property.Attributes["Section"].ToString(), Property.Attributes["Key"].ToString(), Property.DefaultValue.ToString(), stringBuilder, 10000, QuickSettings.FilePath);
			return stringBuilder.ToString();
		}

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern int GetPrivateProfileString(string Section, string Key, string def, StringBuilder retVal, int size, string filePath);

		public void Set(string Section, string Key, string Value)
		{
			IniSettings.WritePrivateProfileString(Section, Key, Value, QuickSettings.FilePath);
		}

		public void Set(SettingsPropertyValue Item)
		{
			IniSettings.WritePrivateProfileString(Item.Property.Attributes["Section"].ToString(), Item.Property.Attributes["Key"].ToString(), Item.PropertyValue.ToString(), QuickSettings.FilePath);
		}

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern long WritePrivateProfileString(string Section, string Key, string Value, string filePath);
	}
}
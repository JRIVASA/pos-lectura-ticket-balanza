using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
	internal static class Program
	{
		public static ParamsHandler Params;

		private static void Main(string[] Args)
		{
			Program.Params = new ParamsHandler(Args);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new ReadDB());
		}
	}
}
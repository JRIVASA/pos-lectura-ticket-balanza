using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using WindowsFormsApplication1.Properties;

namespace WindowsFormsApplication1
{
	internal static class Extensions
	{
		public static void Clean(this WindowsFormsApplication1.Properties.Settings Settings)
		{
			foreach (SettingsPropertyValue propertyValue in Settings.PropertyValues)
			{
				propertyValue.IsDirty = false;
			}
		}

		public static string FillString(this string Text, int NumChars, bool Right = true, char ReplaceChar = ' ')
		{
			string str;
			int numChars = NumChars - Text.Length;
			str = (!Right ? string.Concat(new string(ReplaceChar, (numChars < 0 ? NumChars : numChars)), Text) : string.Concat(Text, new string(ReplaceChar, (numChars < 0 ? NumChars : numChars))));
			return str;
		}

		public static SqlConnection getAlternateConnection(string ServerInstance, string DBName, string UserID, string Password, int ConnectionTimeout = -1)
		{
			return new SqlConnection(string.Concat(Extensions.getAlternateConnectionString(ServerInstance, DBName, UserID, Password, -1), (ConnectionTimeout != -1 ? string.Concat("Connection Timeout = ", ConnectionTimeout, ";") : "")));
		}

		public static string getAlternateConnectionString(string ServerInstance, string DBName, string UserID, string Password, int ConnectionTimeout = -1)
		{
			string[] serverInstance = new string[] { "Server=", ServerInstance, ";Database=", DBName, ";User Id=", UserID, ";Password=", Password, ";" };
			return string.Concat(serverInstance);
		}

		public static bool isUndefined(this string Text)
		{
			bool flag;
			if (Text == null)
			{
				flag = true;
			}
			else
			{
				flag = (Text.Equals(string.Empty) ? true : Text.Equals("undefined", StringComparison.OrdinalIgnoreCase));
			}
			return flag;
		}

		public static void KeepValue(this SettingsPropertyValue Item)
		{
			QuickSettings.Set(Item);
		}

		public static string Left(this string s, int length)
		{
			string str;
			length = Math.Max(length, 0);
			str = (s.Length <= length ? s : s.Substring(0, length));
			return str;
		}

		public static void LoadValue(this SettingsProperty Property, string Section, string Key)
		{
			try
			{
				Settings.Default.PropertyValues[Property.Name].PropertyValue = Convert.ChangeType(QuickSettings.Get(Section, Key, Property), Property.PropertyType);
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
		}

		public static void LoadValue(this SettingsProperty Property)
		{
			try
			{
				Settings.Default.PropertyValues[Property.Name].PropertyValue = Convert.ChangeType(QuickSettings.Get(Property), Property.PropertyType);
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
		}

		public static SqlConnection OpenGet(this SqlConnection Connection)
		{
			try
			{
				if (Connection.State == ConnectionState.Closed)
				{
					Connection.Open();
				}
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
			return Connection;
		}

		public static string Right(this string s, int length)
		{
			string str;
			length = Math.Max(length, 0);
			str = (s.Length <= length ? s : s.Substring(s.Length - length, length));
			return str;
		}
	}
}
using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace WindowsFormsApplication1
{
	internal class ParamsHandler
	{
		private string Delimiter = "|p|";

		private string[] OriginalParameterCollection;

		private string RawParameterString;

		private ArrayList ParameterCollection;

		private string ParamID;

		private bool ParamExists;

		private string ParamValue;

		public string CurrentParameter
		{
			get
			{
				return this.ParamID;
			}
			set
			{
				this.ParamID = value;
				this.LocateParam();
			}
		}

		public bool ParameterExists
		{
			get
			{
				return this.ParamExists;
			}
		}

		public string ParameterValue
		{
			get
			{
				return this.ParamValue;
			}
		}

		public ParamsHandler(string[] Args)
		{
			this.Initialize(Args);
		}

		public ParamsHandler(string[] Args, string Delimiter)
		{
			this.Delimiter = Delimiter;
			this.Initialize(Args);
		}

		private string GetParamValue()
		{
			string empty;
			if (this.ParamID.Length >= this.RawParameterString.Trim().Length)
			{
				empty = string.Empty;
			}
			else
			{
				string str = this.RawParameterString.Substring(this.ParamID.Length + 1);
				empty = str;
			}
			return empty;
		}

		private void Initialize(string[] Args)
		{
			this.OriginalParameterCollection = Args;
			this.RawParameterString = string.Join(" ", Args);
			string rawParameterString = this.RawParameterString;
			string[] delimiter = new string[] { this.Delimiter };
			string[] strArrays = rawParameterString.Split(delimiter, StringSplitOptions.None);
			this.ParameterCollection = new ArrayList();
			if ((strArrays == null ? false : (strArrays.Length > 0)))
			{
				Array.ForEach<string>(strArrays, (string Item) => this.ParameterCollection.Add(Item));
			}
		}

		private bool LocateParam()
		{
			bool paramExists;
			foreach (string parameterCollection in this.ParameterCollection)
			{
				if (parameterCollection.ToUpper().Contains(this.ParamID.ToUpper()))
				{
					if ((!parameterCollection.Contains("=") ? parameterCollection.Equals(this.ParamID, StringComparison.OrdinalIgnoreCase) : parameterCollection.Left(parameterCollection.IndexOf('=')).Equals(this.ParamID, StringComparison.OrdinalIgnoreCase)))
					{
						this.RawParameterString = parameterCollection;
						this.ParamExists = true;
						this.ParamValue = this.GetParamValue();
						paramExists = this.ParamExists;
						return paramExists;
					}
				}
			}
			this.ParamExists = false;
			this.ParamValue = string.Empty;
			paramExists = this.ParamExists;
			return paramExists;
		}
	}
}
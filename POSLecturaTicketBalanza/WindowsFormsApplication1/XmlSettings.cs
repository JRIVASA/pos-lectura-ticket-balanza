using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Xml.Linq;

namespace WindowsFormsApplication1
{
	internal class XmlSettings
	{
		private readonly XmlSettings.ValueReadability VRMode = XmlSettings.ValueReadability.Simple;

		private readonly string WordToIdentifyElements = "Type";

		public XmlSettings()
		{
		}

		public string Get(string Section, string Key, string CustomDefaultValue)
		{
			string str;
			try
			{
				XDocument xDocument = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
				str = this.GetValue(xDocument.Root.Element(QuickSettings.ApplicationName).Element(Section).Element(Key)).ToString();
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				str = CustomDefaultValue;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message.ToString());
				str = CustomDefaultValue;
			}
			return str;
		}

		public object Get(string Section, string Key, SettingsProperty Property)
		{
			object value;
			try
			{
				XDocument xDocument = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
				value = this.GetValue(xDocument.Root.Element(QuickSettings.ApplicationName).Element(Section).Element(Key));
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				value = Property.DefaultValue;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message.ToString());
				value = Property.DefaultValue;
			}
			if (Property.Attributes["Section"] == null)
			{
				Property.Attributes.Add("Section", Section);
			}
			if (Property.Attributes["Key"] == null)
			{
				Property.Attributes.Add("Key", Key);
			}
			return value;
		}

		public object Get(SettingsProperty Property)
		{
			object value;
			try
			{
				XDocument xDocument = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
				value = this.GetValue(xDocument.Root.Element(QuickSettings.ApplicationName).Element(Property.Attributes["Section"].ToString()).Element(Property.Attributes["Key"].ToString()));
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				value = Property.DefaultValue;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message.ToString());
				value = Property.DefaultValue;
			}
			return value;
		}

		private XElement GetNewValue(string Key, string Value, string TypeFullName)
		{
			XElement xElement;
			object[] xAttribute;
			switch (this.VRMode)
			{
				case XmlSettings.ValueReadability.Simple:
				{
					if (!this.WordToIdentifyElements.isUndefined())
					{
						XName key = Key;
						xAttribute = new object[] { new XAttribute(this.WordToIdentifyElements, "Value"), Value };
						xElement = new XElement(key, xAttribute);
						break;
					}
					else
					{
						xElement = new XElement(Key, Value);
						break;
					}
				}
				case XmlSettings.ValueReadability.ValueAsElement:
				{
					if (!this.WordToIdentifyElements.isUndefined())
					{
						XName xName = Key;
						xAttribute = new object[] { new XAttribute(this.WordToIdentifyElements, "Value"), new XElement("Value", Value) };
						xElement = new XElement(xName, xAttribute);
						break;
					}
					else
					{
						xElement = new XElement(Key, new XElement("Value", Value));
						break;
					}
				}
				case XmlSettings.ValueReadability.ValueWithType:
				{
					if (!this.WordToIdentifyElements.isUndefined())
					{
						XName key1 = Key;
						xAttribute = new object[] { new XAttribute(this.WordToIdentifyElements, "Value"), new XElement("Value", Value), new XElement("Type", TypeFullName) };
						xElement = new XElement(key1, xAttribute);
						break;
					}
					else
					{
						XName xName1 = Key;
						xAttribute = new object[] { new XElement("Value", Value), new XElement("Type", TypeFullName) };
						xElement = new XElement(xName1, xAttribute);
						break;
					}
				}
				default:
				{
					xElement = null;
					break;
				}
			}
			return xElement;
		}

		private object GetValue(XElement Element)
		{
			object value;
			switch (this.VRMode)
			{
				case XmlSettings.ValueReadability.Simple:
				{
					value = Element.Value;
					break;
				}
				case XmlSettings.ValueReadability.ValueAsElement:
				case XmlSettings.ValueReadability.ValueWithType:
				{
					value = Element.Element("Value").Value;
					break;
				}
				default:
				{
					value = null;
					break;
				}
			}
			return value;
		}

		public void Set(string Section, string Key, string Value)
		{
			XDocument xDocument;
			try
			{
				xDocument = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				object[] xElement = new object[] { new XElement("Stellar", string.Empty) };
				xDocument = new XDocument(xElement);
			}
			XElement xElement1 = xDocument.Root.Element(QuickSettings.ApplicationName);
			if (xElement1 == null)
			{
				xElement1 = new XElement(QuickSettings.ApplicationName, string.Empty);
				xDocument.Root.Add(xElement1);
				if (!this.WordToIdentifyElements.isUndefined())
				{
					xElement1.SetAttributeValue(this.WordToIdentifyElements, "App");
				}
			}
			XElement xElement2 = xElement1.Element(Section);
			if (xElement2 == null)
			{
				xElement2 = new XElement(Section, string.Empty);
				xElement1.Add(xElement2);
				if (!this.WordToIdentifyElements.isUndefined())
				{
					xElement2.SetAttributeValue(this.WordToIdentifyElements, "Section");
				}
			}
			XElement newValue = xElement2.Element(Key);
			if (newValue != null)
			{
				newValue.ReplaceWith(this.GetNewValue(Key, Value, typeof(string).FullName));
			}
			else
			{
				newValue = this.GetNewValue(Key, Value, typeof(string).FullName);
				xElement2.Add(newValue);
			}
			xDocument.Save(QuickSettings.FilePath, SaveOptions.None);
		}

		public void Set(SettingsPropertyValue Item)
		{
			XDocument xDocument;
			string str = Item.Property.Attributes["Section"].ToString();
			string str1 = Item.Property.Attributes["Key"].ToString();
			try
			{
				xDocument = XDocument.Load(QuickSettings.FilePath, LoadOptions.None);
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				object[] xElement = new object[] { new XElement("Stellar", string.Empty) };
				xDocument = new XDocument(xElement);
			}
			XElement xElement1 = xDocument.Root.Element(QuickSettings.ApplicationName);
			if (xElement1 == null)
			{
				xElement1 = new XElement(QuickSettings.ApplicationName, string.Empty);
				xDocument.Root.Add(xElement1);
				if (!this.WordToIdentifyElements.isUndefined())
				{
					xElement1.SetAttributeValue(this.WordToIdentifyElements, "App");
				}
			}
			XElement xElement2 = xElement1.Element(str);
			if (xElement2 == null)
			{
				xElement2 = new XElement(str, string.Empty);
				xElement1.Add(xElement2);
				if (!this.WordToIdentifyElements.isUndefined())
				{
					xElement2.SetAttributeValue(this.WordToIdentifyElements, "Section");
				}
			}
			XElement newValue = xElement2.Element(str1);
			if (newValue != null)
			{
				newValue.ReplaceWith(this.GetNewValue(str1, Item.PropertyValue.ToString(), Item.Property.PropertyType.FullName));
			}
			else
			{
				newValue = this.GetNewValue(str1, Item.PropertyValue.ToString(), Item.Property.PropertyType.FullName);
				xElement2.Add(newValue);
			}
			xDocument.Save(QuickSettings.FilePath, SaveOptions.None);
		}

		private enum ValueReadability
		{
			Simple,
			ValueAsElement,
			ValueWithType
		}
	}
}
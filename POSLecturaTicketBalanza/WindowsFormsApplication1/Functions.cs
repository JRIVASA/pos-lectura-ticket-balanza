using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using WindowsFormsApplication1.Properties;

namespace WindowsFormsApplication1
{
	public class Functions
	{
		public static string ConnectionData;

		public SqlConnection connectionString = new SqlConnection(Functions.ConnectionData);

		static Functions()
		{
			string[] localDBSQLServerName = new string[] { "Server=", Settings.Default.LocalDB_SQLServerName, ";Database=", Settings.Default.LocalDB_SQLDBName, ";User ID=", Settings.Default.LocalDB_SQLUser, ";Password=", Settings.Default.LocalDB_SQLPass };
			Functions.ConnectionData = string.Concat(localDBSQLServerName);
		}

		public Functions()
		{
		}

		public static void LoadSettings()
		{
			try
			{
				if (!Directory.Exists(QuickSettings.FolderPath))
				{
					Directory.CreateDirectory(QuickSettings.FolderPath);
				}
				if (Settings.Default.PropertyValues.Count <= 0)
				{
					foreach (SettingsProperty settingsProperty in Settings.Default.Properties.OfType<SettingsProperty>())
					{
						SettingsPropertyValue settingsPropertyValue = new SettingsPropertyValue(settingsProperty);
						Settings.Default.PropertyValues.Add(settingsPropertyValue);
					}
				}
				Settings.Default.Properties["LocalDB_SQLServerName"].LoadValue("LocalDB", "SQLServerName");
				Settings.Default.Properties["LocalDB_SQLDBName"].LoadValue("LocalDB", "SQLDBName");
				Settings.Default.Properties["LocalDB_SQLUser"].LoadValue("LocalDB", "SQLUser");
				Settings.Default.Properties["LocalDB_SQLPass"].LoadValue("LocalDB", "SQLPass");
				//se agrega la carga de los nuevos setting 
				Settings.Default.Properties["LocalDB_SQLCASServerName"].LoadValue("LocalDB", "SQLCASServerName");
                Settings.Default.Properties["LocalDB_SQLCASDBName"].LoadValue("LocalDB", "SQLCASDBName");
                Settings.Default.Properties["LocalDB_SQLCASUser"].LoadValue("LocalDB", "SQLCASUser");
                Settings.Default.Properties["LocalDB_SQLCASPass"].LoadValue("LocalDB", "SQLCASPass");
                Settings.Default.Properties["Modalidad"].LoadValue("LocalDB", "Modalidad");

                //fin nuevos setting 
                Settings.Default.Properties["LocalMDB"].LoadValue("LocalDB", "LocalMDB");
				Settings.Default.Properties["LocalEscaner"].LoadValue("Optionals", "escaner");
				Settings.Default.Properties["LocalPosIni"].LoadValue("Optionals", "posIni");
				Settings.Default.Properties["LocalPosFin"].LoadValue("Optionals", "posFin");
				Settings.Default.Clean();
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
		}
	}
}
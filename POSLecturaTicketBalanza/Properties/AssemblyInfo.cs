﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © BIGWISE Corp 2023")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.1.0.0")]
[assembly: AssemblyProduct("POSLecturaTicketBalanza")]
[assembly: AssemblyTitle("POSLecturaTicketBalanza")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("d5f4999a-aad7-4958-a4bc-8a9f7ec2e086")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
